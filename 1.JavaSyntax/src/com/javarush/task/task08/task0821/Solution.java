package com.javarush.task.task08.task0821;

import java.util.HashMap;
import java.util.Map;

/* 
Однофамильцы и тёзки
*/

public class Solution {
    public static void main(String[] args) {
        Map<String, String> map = createPeopleList();
        printPeopleList(map);
    }

    public static Map<String, String> createPeopleList() {
        HashMap<String, String> map = new HashMap<String, String>();

            map.put("Vlad", "Jordan");
            map.put("Kobe", "Bryant");
            map.put("Lebron", "James");
            map.put("Clarkson", "Jordan");
            map.put("Vika", "Jordan");
            map.put("USA", "Jordan");
            map.put("USA", "Jordan");
            map.put("lav", "Jordan");
            map.put("Vlad", "Jordan");
            map.put("Ven", "Jordan");

        return map;
    }

    public static void printPeopleList(Map<String, String> map) {
        for (Map.Entry<String, String> s : map.entrySet()) {
            System.out.println(s.getKey() + " " + s.getValue());
        }
    }
}
