package com.javarush.task.task08.task0820;

import java.util.HashSet;
import java.util.Set;

/* 
Множество всех животных
*/

public class Solution {
    public static void main(String[] args) {
        Set<Cat> cats = createCats();
        Set<Dog> dogs = createDogs();

        Set<Object> pets = join(cats, dogs);
        printPets(pets);

        removeCats(pets, cats);
        printPets(pets);
    }

    public static Set<Cat> createCats() {
        HashSet<Cat> result = new HashSet<Cat>();

        for (int i = 0; i < 4; i++){
            result.add(new Cat());
        }

        return result;
    }

    public static Set<Dog> createDogs() {
        HashSet<Dog> result1 = new HashSet<Dog>();
        for (int i = 0; i < 3; i++){
        result1.add(new Dog());
        }
        return result1;
    }

    public static Set<Object> join(Set<Cat> cats, Set<Dog> dogs) {
        HashSet<Object> set = new HashSet<Object>();

        for (Cat i : cats)
        {
            set.add(i);
        }
        for (Dog i : dogs)
        {
            set.add(i);
        }

        return set;
    }

    public static void removeCats(Set<Object> pets, Set<Cat> cats) {
        HashSet<Object> pets_copy = new HashSet<Object>(pets);

        for (Object i : pets_copy) {
            for (Cat j : cats) {
                if (i.equals(j)) {
                    pets.remove(i);
                }
            }
        }
    }

    public static void printPets(Set<Object> pets) {
        for (Object set : pets)
        System.out.println(set);
    }

    public static class Cat {

    }
    public static class Dog {

    }
}
