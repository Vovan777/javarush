package com.javarush.task.task08.task0823;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* 
Омовение Рамы
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();

        ArrayList<String> list = new ArrayList<String>();
        boolean upDone = false; // если увеличили первый символ слова

        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == ' ' && upDone) {
                upDone = false;
                continue;
            }
            if (s.charAt(i) == ' ') continue;
            if (s.charAt(i) != ' ' && !upDone) {
                s = s.substring(0, i) + ((s.substring(i, i + 1)).toUpperCase()) + s.substring(i + 1);
                upDone = true;
            }

        }

        System.out.println(s);
    }
}
