package com.javarush.task.task04.task0414;

/* 
Количество дней в году
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String sAge = reader.readLine();
        int year = Integer.parseInt(sAge);
        int z = 365;
        if (year % 4 == 0 ^ (year % 100 == 0 && year % 400 != 0))
            System.out.print ("количество дней в году: " + (z + 1));
        else
            System.out.print ("количество дней в году: " + z);

    }
}

