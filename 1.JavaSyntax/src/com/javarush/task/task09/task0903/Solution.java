package com.javarush.task.task09.task0903;

/* 
Кто меня вызывал?
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        method1();
    }

    public static int method1() {

        StackTraceElement[] stackTraceElement = Thread.currentThread().getStackTrace();
        method2();
        return stackTraceElement[2].getLineNumber();
    }

    public static int method2() {

        StackTraceElement[] stackTraceElement = Thread.currentThread().getStackTrace();
        method3();
        return stackTraceElement[2].getLineNumber();
    }

    public static int method3() {

        StackTraceElement[] stackTraceElement = Thread.currentThread().getStackTrace();
        method4();
        return stackTraceElement[2].getLineNumber();
    }

    public static int method4() {

        StackTraceElement[] stackTraceElement = Thread.currentThread().getStackTrace();
        method5();
        return stackTraceElement[2].getLineNumber();
    }

    public static int method5() {

        StackTraceElement[] stackTraceElement = Thread.currentThread().getStackTrace();
        return stackTraceElement[2].getLineNumber();
    }
}
