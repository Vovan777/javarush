package com.javarush.task.task09.task0921;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/* 
Метод в try..catch
*/

public class Solution {
    public static void main(String[] args) {
        readData();
    }

    public static void readData() {
        List<Integer> set = new LinkedList<>();
        try {
            BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
            while (true)//не ограничиваем размер списка
                set.add(Integer.parseInt(r.readLine()));
        } catch (Exception e) {
/*            Iterator it = set.iterator();//рабочий вариант с итератором вместо for-each
            while (it.hasNext()) {//проверка, есть ли ещё элементы
                Object in = it.next();//получение текущего элемента и переход на следующий*/
            for(Integer in: set)
                System.out.println(in);
        }
    }
}
